<?php

namespace Drupal\Tests\webform_ignore_example\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Core\Site\Settings;

/**
 * Tests ExcludedModulesEventSubscriber.
 *
 * @group config
 */
class WebformIgnoreExampleEventSubscriberTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'webform_ignore_example',
    'webform_ignore_example_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system', 'webform_ignore_example_test']);
  }

  /**
   * Test excluding modules from the config export.
   */
  public function testExcludedModules() {
    // Assert that our webform config is in the active config.
    $active = $this->container->get('config.storage');
    $this->assertNotEmpty($active->listAll('webform.webform.'));
    $this->assertNotEmpty($active->listAll('system.'));
    // Add collections.
    $collection = $this->randomMachineName();
    foreach ($active->listAll() as $config) {
      $active->createCollection($collection)->write($config, $active->read($config));
    }

    // Assert that webform config is not in the export storage.
    $export = $this->container->get('config.storage.export');
    $this->assertEmpty($export->listAll('webform.webform.'));
    $this->assertNotEmpty($export->listAll('system.'));
    // And assert excluded from collections too.
    $this->assertEmpty($export->createCollection($collection)->listAll('webform.webform.'));
    $this->assertNotEmpty($export->createCollection($collection)->listAll('system.'));

    // Assert that existing webform_config is again in the import storage.
    $import = $this->container->get('config.import_transformer')->transform($export);
    $this->assertNotEmpty($import->listAll('webform.webform.'));
    $this->assertNotEmpty($import->listAll('system.'));

    // Enable export.
    $settings = Settings::getInstance() ? Settings::getAll() : [];
    $settings['webform_ignore_example_stage_site'] = TRUE;
    new Settings($settings);
    drupal_flush_all_caches();
    // Assert that webform config is in the export storage.
    $export = $this->container->get('config.storage.export');
    $this->assertNotEmpty($export->listAll('webform.webform.'));
    $this->assertNotEmpty($export->listAll('system.'));
    // And assert excluded from collections too.
    $this->assertNotEmpty($export->createCollection($collection)->listAll('webform.webform.'));
    $this->assertNotEmpty($export->createCollection($collection)->listAll('system.'));

    // Assert config not removed if it exists in exported storage.
    $settings = Settings::getInstance() ? Settings::getAll() : [];
    $settings['webform_ignore_example_stage_site'] = FALSE;
    new Settings($settings);
    drupal_flush_all_caches();
    $sync = $this->container->get('config.storage.sync');
    $active = $this->container->get('config.storage');
    // Store sync storage, and make changes to active storage.
    $this->copyConfig($active, $sync);
    $active_webform_config = $active->read('webform.webform.exclude_test');
    $active_weform_config['title'] = 'Updated';
    $active->write('webform.webform.exclude_test', $active_webform_config);
    $active_system_config = $active->read('system.site');
    $active_system_config['name'] = 'Updated';
    $active->write('system.site', $active_system_config);
    // Assert that webform config remains as is,
    // but update to sytem is exported.
    $export = $this->container->get('config.storage.export');
    $export_webform_config = $export->read('webform.webform.exclude_test');
    $this->assertEquals($export_webform_config['title'], 'Test');
    $export_system_config = $export->read('system.site');
    $this->assertEquals($export_system_config['name'], 'Updated');

    // Assert config is imported if present.
    $import = $this->container->get('config.import_transformer')->transform($export);
    $import_webform_config = $import->read('webform.webform.exclude_test');
    $this->assertEquals($import_webform_config['title'], 'Test');
  }

}
