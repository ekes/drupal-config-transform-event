# Transform your Drupal configuration on import and export

Notes about implementing custom configuration ignore using the [config transformation event introduced in 8.8.0](https://www.drupal.org/node/3066005). Core's config exclude is too blunt for what you want, config ignore or config split don't quite fit your particular rules. Sometimes just writing code a bit of code is easier than writing config. 

## Background

The example given below started with the requirement to generally ignore webforms, a common enough request, but to allow one stage site to export them to configuration, and all sites to import these new webforms. Config ignore of course presently [only works on the import](https://www.drupal.org/project/config_ignore/issues/2857247). I can already see it will be helpful for a multi-site that has a split, but some sites are installed in a different primary language and so mustn't export to the default collection as this overrides the configuration language on all the shared config.

There is a Config Ignore 3.x in the works, and it does ignore on import and export. It was also [here that exactly how the new config storage transformation events work and can be used](https://git.drupalcode.org/project/config_ignore/-/blob/8.x-3.x/src/EventSubscriber/ConfigIgnoreEventSubscriber.php) became apparent. However, the branch still has issues, [working for wildcards, with all the nested config is clearly not trivial](https://www.drupal.org/project/config_ignore/issues/3117694). In the example you'll see I've used the variable naming from the 3.x branch of config ignore. It's this `destination_collection` and `transformation_collection` that made clear which configuration you get from which service and through the event itself. Variable names as code documenting itself.

In core there is the config exclude which can be used like [$settings['config_exclude_modules'] = ['devel', 'stage_file_proxy'];](https://www.drupal.org/node/3079028). But as noted it is a bit blunt, not only because it does it for all the module configuration, but also as [it just deletes the configuration on export](https://git.drupalcode.org/project/drupal/-/blob/9.0.x/core/lib/Drupal/Core/EventSubscriber/ExcludedModulesEventSubscriber.php#L130). [It maintains any configuration on import](https://git.drupalcode.org/project/drupal/-/blob/9.0.x/core/lib/Drupal/Core/EventSubscriber/ExcludedModulesEventSubscriber.php#L88) by writing over the configuration being imported with the configuration that is actually active. Now we could also do that on export to leave exported configuration alone.

Note the one added complexity in the core code. It would seem to make sense to just loop over all the config name keys and check them in the appropriate storage. But the [storage can be split up into collections](https://www.drupal.org/node/2268523). Collections are commonly used for configuration in different languages. So the code does the check for each collection rather than over the storage directly.

## Code

### EventSubscriber

Declaring the subscriber in our `webform_ignore_example.libraries.yml`

```yml
services:
  webform_ignore_example.event_subscriber:
    class: Drupal\webform_ignore_example\EventSubscriber\ConfigSubscriber
    arguments: ['@config.storage', '@config.storage.sync']
    tags:
      - { name: event_subscriber }
```

and then implementing a standard event subscriber that will react to the storage transform events on import and export.

```php
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\StorageTransformEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ConfigSubscriber implements EventSubscriberInterface {

  /**                                         
   * The active config storage.                 
   *                                                                                                      
   * @var \Drupal\Core\Config\StorageInterface                                                            
   */              
  protected $activeStorage;                                                                               
                                                                                                          
  /**                                            
   * The sync config storage.                                                                             
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $syncStorage;

  /**
   * DirectoriesConfigSubscriber constructor.
   *
   * @param \Drupal\Core\Config\StorageInterface $config_storage
   *   The config active storage.
   * @param \Drupal\Core\Config\StorageInterface $sync_storage
   *   The sync config storage.
   */
  public function __construct(StorageInterface $config_storage, StorageInterface $sync_storage) {
    $this->activeStorage = $config_storage;
    $this->syncStorage = $sync_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::STORAGE_TRANSFORM_IMPORT][] = ['onImportTransform'];
    $events[ConfigEvents::STORAGE_TRANSFORM_EXPORT][] = ['onExportTransform'];
    return $events;
  }
  
  /**
   * The storage is transformed for importing.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onImportTransform(StorageTransformEvent $event) {
    $transformation_storage = $event->getStorage();
    assert($transformation_storage instanceof StorageInterface);                   
    $destination_storage = $this->activeStorage;

  }

  /**
   * The storage is transformed for exporting.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onExportTransform(StorageTransformEvent $event) {
    $transformation_storage = $event->getStorage();
    $destination_storage = $this->syncStorage;

  }
}
```

Notice the included active and sync storage from their services, these are then available to get the current state of the destination on import and export.

### Import

Our aim is to import any new webforms, but not delete any that have been made in the site and not been exported to the configuration.

At its simplest we could just check in the storage for the configuration we are looking for:

```php
    $transformation_storage = $event->getStorage();
    $destination_storage = $this->activeStorage;

    // Loop over an array of the names of webform configuration that exist in the destination.
    // More about the method to get the list later.
    foreach ($this->ignoreConfigNames($destination_storage) as $config_name) {
      // If there is a configuration in the destination storage (the database)
      // which is not in the transformation storage (from directory) don't delete
      // it.
      if (!$transformation_storage->exists($config_name)) {
        // Replace the missing configuration from the existing destination configuration.
        $transformation_storage->write($config_name, $destination_storage->read($config_name));
      }
    }
```

But as mentioned above it can be split up into collections. [So following core](https://git.drupalcode.org/project/drupal/-/blob/9.0.x/core/lib/Drupal/Core/EventSubscriber/ExcludedModulesEventSubscriber.php#L82):-

```php
    // We first loop over all the collections, and work on each collection.
    foreach (array_merge([StorageInterface::DEFAULT_COLLECTION], $this->activeStorage->getAllCollectionNames()) as $collection_name) {
      $transformation_collection = $transformation_storage->createCollection($collection_name);
      $destination_collection = $destination_storage->createCollection($collection_name);     
      foreach ($this->ignoreConfigNames($destination_storage) as $config_name) {
        if (!$transformation_collection->exists($config_name) && $destination_collection->exists($config_name)) {
          // Make sure the config is not removed if it exists.
          $transformation_collection->write($config_name, $destination_collection->read($config_name));
        }           
      }                               
    }
```

### Export

On export for most sites we don't want to write any of the ignored configuration.
Inside the same loop of collections, retrieving the list of configuration names in the content being exported:-

```php
      foreach ($this->ignoreConfigNames($transformation_storage) as $config_name) {
        // If a configuration exists in the destination (configuration directory)
        // already leave it as it is, even if there are changes in the
        // transformation_storage (from the database).
        if ($destination_collection->exists($config_name)) {
          $transformation_collection->write($config_name, $destination_collection->read($config_name));
        }                                  
        else {                         
          // Otherwise the configuration does not exist in the destination
          // (configuration directory) and should not be written to it from the
          // transformation_storage (as it is in the database).
          $transformation_collection->delete($config_name);
        }
```

Of course on one site we do want to export. We'll put a `$setting['webform_ignore_example_stage_site'] = TRUE` and [get it with \Drupal\Core\Site\Settings](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Site%21Settings.php/function/Settings%3A%3Aget/9.0.x) value in `settings.local.php` that's not deployed to any other site.

```php
  /**
   * The storage is transformed for exporting.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onExportTransform(StorageTransformEvent $event) {
    // Export all changes if $settings['webform_ignore_example_stage_site'] = TRUE.
    if (Settings::get('webform_ignore_example_stage_site')) {
      return;
    }

    $transformation_storage = $event->getStorage();
    $destination_storage = $this->syncStorage;

    foreach (array_merge([StorageInterface::DEFAULT_COLLECTION], $transformation_storage->getAllCollectionNames()) as $collection_name) {
      $transformation_collection = $transformation_storage->createCollection($collection_name);
      $destination_collection = $destination_storage->createCollection($collection_name);
      foreach ($this->ignoreConfigNames($transformation_storage) as $config_name) {
```

### Get a list of configuration names

[Config exclude in core retrieves all the configuration name for the listed modules, and all their dependencies](https://git.drupalcode.org/project/drupal/-/blob/9.0.x/core/lib/Drupal/Core/EventSubscriber/ExcludedModulesEventSubscriber.php#L168). We just want to get the names of webforms, and maybe webform_options, that have been set. Using the same [StorageInterface::listAll](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Config%21StorageInterface.php/function/StorageInterface%3A%3AlistAll/9.0.x) for our set prefixes.

```php
  /**               
   * Configuration prefixes to ignore.
   * 
   * @var array
   */
  const PREFIXES = ['webform.webform.', 'webform.webform_options.'];

  /**
   * List all the configuration names to ignore in the storage.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   The config storage to retreive all the key names from.
   *
   * @return string[]
   *   Yields the key names matching in the storage.
   */
  protected function ignoreConfigNames(StorageInterface $storage) {
    foreach (self::PREFIXES as $prefix) {
      foreach ($storage->listAll($prefix) as $name) { 
        yield $name;
      }
    }
  }
```

### Full code

[The event subscriber](./src/EventSubscriber/ConfigSubscriber.php).
The full module should install, and there is [a test](./tests/src/Kernel/WebformIgnoreExampleEventSubscriberTest.php) that confirms the event subscriber works.
